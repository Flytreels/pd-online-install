# proot-distro-cn
为termux的中文简体用户提供本地化proot-distro体验

支持arm32,arm64,amd64设备

## 一键在线安装
`curl https://gitee.com/Flytreels/pd-online-install/raw/master/install.sh | bash`

## 开发来源
其源代码源自Termux 官方proot-distro版本
经过优化后更适合中文简体用户使用

### 开源许可证
遵循上游仓库**GPL-V3**,具体内容请查看LICENSE文件

----
警告：此版本并非termux官方指派发布，与termux 官方无任何关系，仅为个人开发